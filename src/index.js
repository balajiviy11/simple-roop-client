/**
 * Basic Class creation and exporting 

  export class Car {
    constructor() {
      console.log("nothing to do with this!");
    }
    async one() {console.log("one is called!")}
    async two() {}
  }
  
 */

import { xml, jid, client } from "@xmpp/client";
import EventEmitter from "events";

export class FaekoClient extends EventEmitter {
  constructor(options = {}, ...args) {
    // set the standard
    options.standard = "http://hool.org/protocol/mug/roop";

    super(...args);
    if (!options.service) {
      console.warn("No XMPP service specified. We won't be able to connect!");
    } else {
      this.service = options.service;
    }
    if (!options.standard) {
      console.warn(
        "No game standard specified. Using generic Multi-User Gaming, which may include incompatible game hosts"
      );
      this.standard = "http://jabber.org/protocol/mug";
    } else {
      this.standard = options.standard;
    }
    this.xmpp = null;
    this.jid = null;
    this.resource = options.resource || Math.random().toString(16).substr(2, 8);
  }

  /**
   * Game-level listeners
   */
  handleGamePresence(presence) {
    let game = presence.getChild("game");

    if (game.attrs.xmlns == "http://jabber.org/protocol/mug") {
      // Handle game errors
      if (presence.getChildren("error").length) {
        let result = {
          room: jid(presence.attrs.from).bare(),
          user: jid(presence.attrs.to).bare(),
          error: {},
        };
        let error = presence.getChild("error");

        if (error.attrs.type == "wait") {
          result.error.type = "wait";

          // Handle "room full" event
          if (error.getChildren("service-unavailable").length) {
            if (
              error.getChild("service-unavailable").attrs.xmlns ==
              "urn:ietf:params:xml:ns:xmpp-stanzas"
            ) {
              result.event = "roomFull";
              return result;
            }
          }
        }
      }

      // Handle state update
      let status;
      if (game.getChildren("status").length) {
        status = game.getChild("status");
      }

      if (status || game.getChildren("state").length) {
        let state = {};

        state.room = jid(presence.attrs.from).bare().toString();

        // add in the status
        if (status) state.status = status.text();

        let xData = game
          .getChild("state")
          .getChildren("x")
          .find((x) => x.attrs.xmlns == "jabber:x:data");

        if (xData) {
          // Now the actual processing begins!

          // get deck info
          if (!!xData.getChildren("deck").length) {
            let deckEl = xData.getChild("deck");
            state.deck = [];

            // trump
            if (deckEl.attrs.trump) {
              state.trump = deckEl.attrs.trump;
            }

            // cards
            deckEl.children.forEach((cardEl) =>
              state.deck.push({
                suit: cardEl.attrs.suit,
                rank: cardEl.attrs.rank,
              })
            );
          }

          // get player info
          if (!!xData.getChildren("players").length) {
            let playersEl = xData.getChild("players");
            state.players = [];

            playersEl.getChildren("player").forEach((playerEl, i) => {
              let player = {};

              if (playerEl.attrs.inactive == "true") {
                player.inactive = true;
              } else if (playerEl.attrs.inactive == "false") {
                player.inactive = false;
              }
              // get hand
              if (playerEl.getChildren("hand").length) {
                let handEl = playerEl.getChild("hand");
                player.hand = [];

                // whether the hand is exposed
                if (handEl.attrs.exposed == "true") {
                  player.exposed = true;
                } else if (handEl.attrs.exposed == "false") {
                  player.exposed = false;
                }

                // the hand itself!
                handEl.getChildren("card").forEach((c) =>
                  player.hand.push({
                    suit: c.attrs.suit,
                    rank: c.attrs.rank,
                  })
                );
              }

              // get bid
              if (playerEl.getChildren("bid").length) {
                let bidEl = playerEl.getChild("bid");

                player.bid = Number(bidEl.text());

                // If it's empty, make it "true". That means
                // we know there's a bid but don't know what
                // it is.
                if (isNaN(player.bid)) {
                  player.bid = true;
                }
              }

              // get score (if any)
              if (playerEl.getChildren("score").length) {
                let scoreEl = playerEl.getChild("score");

                player.score = Number(scoreEl.text());
              }

              // respect the index, if specified
              if (player.index) i = player.index;

              // add the player to the list
              state.players[i] = player;
            });

            // get trick info
            if (!!xData.getChildren("tricks").length) {
              let tricksEl = xData.getChild("tricks");
              state.tricks = [];
              state.flippedTricks = [];

              tricksEl.getChildren("trick").forEach((trickEl) => {
                let trick = [];

                trickEl.getChildren("card").forEach((cardEl) => {
                  trick.push({
                    suit: cardEl.attrs.suit,
                    rank: cardEl.attrs.rank,
                    player: cardEl.attrs.player,
                  });
                });

                state.tricks.push(trick);

                // Check the flip value
                if (!!trickEl.attrs.flipped) {
                  state.flippedTricks.push(true);
                } else {
                  state.flippedTricks.push(false);
                }
              });
              //setting trump which is come from the presence to the STATE.TRICKS
              let trump = tricksEl.getChildren("trump")[0];
              state.tricks.push(trump);
            }
          }
        }

        // Return it
        state.event = "state";
        return state;
      }
    }

    return;
  }
  handleGameTurn(message) {
    let turn = message.getChild("turn");

    // Handle card
    if (turn.getChildren("card").length) {
      let cardEl = turn.getChild("card");

      // Ignore non-Faeko cards
      if (cardEl.attrs.xmlns != this.standard) return;

      // Detect errors
      if (message.getChildren("error").length) {
        let errorEl = message.getChild("error");

        let error = {};

        // Figure out error type
        if (errorEl.getChildren("forbidden").length) {
          error.type = "not-authorised";
        } else if (errorEl.getChildren("invalid-turn").length) {
          error.type = "invalid-turn";
        } else {
          error.type = "unknown";
        }

        // Get error text
        if (errorEl.getChildren("text").length) {
          error.text = errorEl.getChild("text").text();
        }

        error.event = "cardError";
        return error;
      }

      // If no errors, return the normal card!
      let card = {};
      card.room = jid(message.attrs.from).bare().toString();
      card.nickname = jid(message.attrs.from).resource;
      card.rank = cardEl.attrs.rank;
      card.suit = cardEl.attrs.suit;
      card.player = cardEl.attrs.player;

      card.event = "card";
      return card;
    }
  }
  async xmppConnect(myJID, password = undefined) {
    this.jid = jid(myJID);
    if (this.xmpp) {
      await this.xmpp.stop();
    }
    if (!password) {
      this.xmpp = client({
        service: this.service,
        domain: myJID,
        resource: this.resource,
      });
    } else {
      this.xmpp = client({
        service: this.service,
        domain: this.jid.domain,
        resource: this.resource,
        username: this.jid.local,
        password: password,
      });
    }
    this.listen(this.xmpp);
  }
  async listen(connection) {
    const { iqCaller } = connection;
    connection.on("error", (err) => {
      let error = {};
      error.code = err.code;
      if (err.code == "ECONNERROR") {
        error.message = "Connection error. Please try again";
      } else if (err.condition == "not-authorized") {
        error.code = "not-authorized";
        error.message = "Invalid username or password";
      } else {
        error.message = "Unknown error";
      }
      this.emit("error", error);
    });
    connection.on("offline", () => {
      console.log("offline");
      this.emit("offline", {});
    });
    connection.on("stanza", async (stanza) => {
      console.log(`We got: ${stanza}`);
      let from = stanza.attrs.from ? jid(stanza.attrs.from) : undefined;
      if (from && from.local == this.jid.local && from.host == this.jid.host) {
        console.log("ignoring own message");
        return;
      }
      if (stanza.is("presence")) {
        if (stanza.getChildren("game").length) {
          let e = this.handleGamePresence(stanza);
          if (!!e) {
            this.emit(e.event, e);
            return;
          } else {
            let game = stanza.getChild("game");
            let role;
            let affiliation;
            let room = jid(stanza.attrs.from).bare().toString();
            let nickname = jid(stanza.attrs.from).resource;
            let user = nickname;
            if (game.getChildren("item").length) {
              let item = game.getChild("item");
              role = item.attrs.role;
              affiliation = item.attrs.affiliation || unknown;
              user = item.attrs.jid;
            }
            if (stanza.attrs.type == "unavailable") {
              if (
                game.getChildren("item").length &&
                game.getChild("item").attrs.affiliation == "none"
              ) {
                this.emit("roomExit", {
                  user: user,
                  nickname: nickname,
                  room: room,
                });
                return;
              }
              this.emit("roomDisconnect", {
                user: user,
                room: room,
              });
              return;
            }
            if (stanza.getChildren("error").length) {
              let error = stanza.getChild("error");
              this.emit("roomJoinError", {
                room: room,
                user: user,
                nickname: nickname,
                type: error.attrs.type || "unknown",
                tag: error.children[0].name,
                text: error.getChild("text").getText(),
              });
              return;
            }
            this.emit("roomJoin", {
              room: room,
              user: user,
              role: role,
              affiliation: affiliation,
              nickname: nickname,
            });
            return;
          }
        } else if (stanza.attrs.xmlns == "jabber:client") {
          if (stanza.attrs.type == "subscribe") {
            this.emit("roster-subscribe", {
              user: stanza.attrs.from,
            });
          } else if (stanza.attrs.type == "subscribed") {
            this.emit("roster-subscribed", {
              user: stanza.attrs.from,
            });
          } else if (stanza.attrs.type == "unsubscribe") {
            this.emit("roster-unsubscribe", {
              user: stanza.attrs.from,
            });
          } else if (stanza.attrs.type == "unsubscribed") {
            this.emit("roster-unsubscribed", {
              user: stanza.attrs.from,
            });
          } else if (stanza.attrs.type == "unavailable") {
            this.emit("contact-offline", {
              user: stanza.attrs.from,
            });
          } else {
            let show;
            let status;
            if (stanza.getChildren("show").length) {
              show = stanza.getChild("show").text();
            }
            if (stanza.getChildren("status").length) {
              status = stanza.getChild("status").text();
            }
            this.emit("contact-online", {
              user: stanza.attrs.from,
              show: show,
              status: status,
            });
          }
        }
      }
      if (!stanza.is("message")) return;
      if (stanza.is("message")) {
        if (stanza.getChildren("turn").length) {
          let e = this.handleGameTurn(stanza);
          if (!!e) {
            this.emit(e.event, e);
            return;
          }
          return;
        }
        if (stanza.getChildren("start").length) {
          let u = jid(stanza.attrs.from);
          if (stanza.getChildren("error").length) {
            this.emit("startError", {
              game: `${u.bare()}`,
              user: u.resource,
            });
          } else {
            this.emit("start", {
              game: `${u.bare()}`,
              user: u.resource,
            });
          }
        }
        if (stanza.getChildren("game").length) {
          let game = stanza.getChild("game");
          if (game.attrs.xmlns != "http://jabber.org/protocol/mug#user") {
            console.warn(`Dropping unrecognised stanza: ${stanza}`);
          }
          let invited = game.getChild("invited");
          if (invited && invited.attrs.var == this.standard) {
            let reason = invited.getChild("reason") || null;
            if (reason) reason = reason.text();
            let role = null;
            if (game.getChild("item")) {
              role = game.getChild("item").attrs.role || null;
            }
            this.emit("invited", {
              invitor: invited.attrs.from,
              invitee: stanza.attrs.to,
              role: role,
              game: stanza.attrs.from,
              reason: reason,
            });
          }
          let declined = game.getChild("declined");
          if (declined) {
            let reason = declined.getChild("reason") || null;
            if (reason) reason = reason.text();
            let role = null;
            if (game.getChild("item")) {
              role = game.getChild("item").attrs.role || null;
            }
            this.emit("invite-declined", {
              invitee: stanza.attrs.from,
              invitor: declined.attrs.to,
              game: stanza.attrs.from,
              reason: reason,
              role: role,
            });
          }
        }
        if (
          stanza.attrs.type == "groupchat" &&
          stanza.getChildren("body").length
        ) {
          let sender = stanza.attrs.from.split("/");
          this.emit("groupchat", {
            from: stanza.attrs.from,
            to: stanza.attrs.to,
            user: sender[1],
            game: sender[0],
            id: stanza.attrs.id,
            text: stanza.getChild("body").text(),
          });
        } else if (
          stanza.attrs.type == "chat" &&
          stanza.getChildren("body").length
        ) {
          this.emit("chat", {
            from: stanza.attrs.from,
            to: stanza.attrs.to,
            id: stanza.attrs.id,
            text: stanza.getChild("body").text(),
            nickname: jid(stanza.attrs.from).resource,
          });
        }
      }
    });
    connection.on("online", async (address) => {
      console.log("...and we're on!");
      this.jid = this.xmpp.jid;
      await connection.send(
        xml(
          "presence",
          null,
          xml("show", null, "chat"),
          xml("status", null, "Hi, I'm a test user")
        )
      );
      console.log("connected to xmpp server");
      this.emit("connected", {
        status: "ok",
      });
    });
    console.log("All set up. Trying to connect...");
    connection.start().catch(console.error);
  }
  async xmppStop() {
    return await this.xmpp.stop();
  }
  async xmppDiscoverServices() {
    console.log("discovering services");
    let response = await this.xmpp.iqCaller.request(
      xml(
        "iq",
        {
          type: "get",
        },
        xml("query", {
          xmlns: "http://jabber.org/protocol/disco#info",
        })
      )
    );
    if (
      response.getChild("query").children.some((e) => {
        return (
          e.name == "feature" &&
          e.attrs.var == "http://jabber.org/protocol/disco#items"
        );
      })
    ) {
      console.log("disco#items supported");
      response = await this.xmpp.iqCaller.request(
        xml(
          "iq",
          {
            type: "get",
          },
          xml("query", {
            xmlns: "http://jabber.org/protocol/disco#items",
          })
        )
      );
      var hosts = [];
      for await (let service of response.getChild("query").children) {
        if (service.name == "item") {
          console.log(`checking: ${service.attrs.jid}`);
          try {
            response = await this.xmpp.iqCaller.request(
              xml(
                "iq",
                {
                  type: "get",
                  to: service.attrs.jid,
                },
                xml("query", {
                  xmlns: "http://jabber.org/protocol/disco#info",
                })
              )
            );
            if (
              response.getChild("query").children.some((feature) => {
                return (
                  feature.name == "feature" &&
                  feature.attrs.var == this.standard
                );
              })
            ) {
              console.log(`Found game service at ${service.attrs.jid}`);
              hosts.push(service.attrs.jid);
            }
          } catch (e) {
            console.log(`Warning: skipping ${service.attrs.jid}: ${e}`);
          }
        }
      }
      if (hosts.length < 1) {
        console.log("Could not find any hosts for this game :(");
        return;
      }
      console.log(`Found hosts: ${hosts}`);
      this.gameService = hosts[0];
      return this.gameService;
    } else {
      console.log("disco#items not supported here :(");
      return;
    }
  }
  async join(gameID, role, nick = undefined) {
    if (!gameID) throw "Please specify a gameID";
    if (gameID.indexOf("@") == -1) gameID = `${gameID}@${this.gameService}`;
    gameID = jid(gameID);
    if (nick) gameID.resource = nick;
    this.xmpp.send(
      xml(
        "presence",
        {
          from: this.jid,
          to: gameID,
        },
        xml(
          "game",
          {
            var: this.standard,
          },
          xml("item", {
            role: role,
          })
        )
      )
    );
  }
  leave(gameID) {
    if (!gameID) throw "Please specify a gameID";
    if (gameID.indexOf("@") == -1) gameID = `${gameID}@${this.gameService}`;
    this.xmpp.send(
      xml(
        "presence",
        {
          from: this.jid,
          to: gameID,
          type: "unavailable",
        },
        xml(
          "game",
          {
            xmlns: "http://jabber.org/protocol/mug",
          },
          xml("item", {
            affiliation: "none",
            role: "none",
            jid: this.jid,
          })
        )
      )
    );
  }
  start(gameID) {
    if (!gameID) throw "Please specify a gameID";
    if (gameID.indexOf("@") < 0) gameID = `${gameID}@${this.gameService}`;
    this.xmpp.send(
      xml(
        "message",
        {
          from: this.jid,
          to: gameID,
        },
        xml("start", {
          xmlns: "http://jabber.org/protocol/mug#user",
        })
      )
    );
  }
  chat(userJID, text) {
    if (!userJID) throw "Please specify a user JID!";
    this.xmpp.send(
      xml(
        "message",
        {
          from: this.jid,
          to: userJID,
          type: "chat",
        },
        xml("body", null, text)
      )
    );
  }
  groupChat(gameID, text) {
    if (!gameID) throw "Please specify a gameID";
    if (gameID.indexOf("@") == -1) gameID = `${gameID}@${this.gameService}`;
    this.xmpp.send(
      xml(
        "message",
        {
          from: this.jid,
          to: gameID,
          type: "groupchat",
        },
        xml("body", null, text)
      )
    );
  }
}
